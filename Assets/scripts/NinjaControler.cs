﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class NinjaControler : MonoBehaviour {
	// Variables publicas para poder modifircar desde el inspector
	public float maxS = 11f;

	//Variables privadas
	private Rigidbody2D rb2d = null;
	private float move = 0f;
	private Animator anim;
	private bool flipped = false;
	public AudioSource sword;
	public AudioSource jump;
	public AudioSource dying;








	// Use this for initialization
	void Awake () {
		// Obtenemos el rigidbody y lo guardamos en la variable rb2d
		// para poder utilizarla más cómodamente
		rb2d = GetComponent<Rigidbody2D>();

		// Obtenemos el Animator Controller para poder modificar sus variables
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		

		//Miramos el input Horizontal
		move = Input.GetAxis ("Horizontal");
	

		rb2d.velocity = new Vector2 (move * maxS, rb2d.velocity.y);

		//Miramos si nos estamos moviendo.
		// OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
		if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f) {
			if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped)) {
				flipped = !flipped;
				this.transform.rotation = Quaternion.Euler (0, flipped ? 180 : 0, 0);
			}
			anim.SetBool ("run", true);
		} else {
			anim.SetBool ("run", false);
		}

		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			anim.SetBool ("atack", true);
			sword.Play();

		} else if (Input.GetKeyUp (KeyCode.Mouse0)) {
			anim.SetBool ("atack", false);
			//anim.SetBool ("atackbool", false);
		}

		if (Input.GetKeyDown(KeyCode.Space))
			jump.Play();
			anim.SetBool ("jump", false);
		
		//anim.SetBool ("jump", true);

		if (Input.GetKeyUp (KeyCode.Space)) {
			anim.SetBool ("jump", true);
			rb2d.velocity = new Vector3(0, 7, 0);
		}



			}
	

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "die") {
			dying.Play();
			anim.SetBool ("dead", true);
			Invoke ("Gg", 2.0f);


		
			
		}

		if (other.tag == "win") {
			
			SceneManager.LoadScene("win");
		}

	}
	void Gg(){
		SceneManager.LoadScene("GameOver");

	}

	}


