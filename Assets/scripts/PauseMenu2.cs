using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu2 : MonoBehaviour {

	public static bool GameIsPaused = false;
	public GameObject pauseUI;
	// Use this for initialization

	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (GameIsPaused) {
				Resume ();
			} else {
				Pause ();
			}
		}

	}
	void Resume(){
		pauseUI.SetActive (false);
		Time.timeScale = 1f;
		GameIsPaused = false; 

	}

	void Pause(){

		pauseUI.SetActive (true);
		Time.timeScale = 0f;
		GameIsPaused = true;


	}


	public void ExitButton(){
		Application.Quit();
	}

	public void MenuButton(){
		Time.timeScale = 1f;
		SceneManager.LoadScene("Title");
	}
	public void PauseButton(){
		Resume ();
	}
}

