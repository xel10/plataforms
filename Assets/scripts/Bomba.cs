﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : MonoBehaviour {
	private Animator anim;



	void Awake () {
		// Obtenemos el rigidbody y lo guardamos en la variable rb2d
		// para poder utilizarla más cómodamente


		// Obtenemos el Animator Controller para poder modificar sus variables
		anim = GetComponent<Animator>();
	}





	void OnTriggerEnter2D(Collider2D other){

		if (other.tag == "Player") {
			
			anim.SetBool ("explosion", true);
		}





		if (other.tag == "destroy") {
			
			Destroy (gameObject);

		}
}
}
